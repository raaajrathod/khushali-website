AOS.init();

$(document).ready(function () {
  // Define selector for selecting
  // anchor links with the hash
  let anchorSelector = 'a[href^="#"]';

  $(anchorSelector).on("click", function (e) {
    // Prevent scrolling if the
    // hash value is blank
    e.preventDefault();
    if (!e.target.attributes.href.nodeValue.startsWith("#section_")) {
      return false;
    }
    $(".toggle-holder").click();

    // Get the destination to scroll to
    // using the hash property
    let destination = $(this.hash);

    // Get the position of the destination
    // using the coordinates returned by
    // offset() method and subtracting 50px
    // from it.
    let scrollPosition = destination.offset().top - 75;

    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if (isMobile) {
      /* your code here */
      scrollPosition = destination.offset().top - 35;
    }

    // Specify animation duration
    let animationDuration = 500;

    // Animate the html/body with
    // the scrollTop() method
    $("html, body").animate(
      {
        scrollTop: scrollPosition,
      },
      animationDuration
    );
  });
});
var coll = document.getElementsByClassName("collapsible");
var i;
var toggleHeaderBtn = $(".toggle-holder");
var menuWrapper = document.querySelector(".header-right-part .menu-wrapper");

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
      //   content.style.paddingBottom = "20px"
    }
  });
}

$(".toggle-holder").click(function (e) {
  e.stopPropagation();
  menuWrapper.classList.toggle("active");
});
